package week3.homework3;

public interface Observer {
     void update(float temp, float humid, float press);
}
