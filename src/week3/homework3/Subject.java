package week3.homework3;

public interface Subject {
    void registerObserver(Observer o);
    void removeObserver( Observer o);
    void notifyObserver();
}
