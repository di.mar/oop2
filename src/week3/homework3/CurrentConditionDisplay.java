package week3.homework3;

public class CurrentConditionDisplay implements Observer, DisplayElement {

    private float temperature;
    private float humidity;
    private float pressure;
    private Subject weatherData;

    public CurrentConditionDisplay(WeatherData weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }



    @Override
    public void display() {
        System.out.println("Current conditions: " + temperature  + "F degrees and " + humidity + "% humidity "+ pressure+"f pressure");
    }

    @Override
    public void update(float temp, float humid, float press) {
        this.temperature = temp;
        this.humidity = humid;
        this.pressure = press;
        display();
    }
}
