package week9.homework9;

public abstract class IceCreamStore {

    protected abstract IceCream makeIceCream(String type);

    public IceCream orderIceCream(String type){
        IceCream iceCream;
        iceCream = makeIceCream(type);

        iceCream.preparing();
        iceCream.freezing();
        iceCream.packaging();

        System.out.println("Thank you for your order! This is your ice cream.");
        return iceCream;

    }
}
