package week9.homework9;

public class KoreaChocolateIceCream extends IceCream {
    public KoreaChocolateIceCream() {
        this.setName("Korea styled Ice Cream");
        this.setFlavor("Chocolate");
        this.setType("Pancake");
    }
}
