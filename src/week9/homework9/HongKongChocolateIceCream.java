package week9.homework9;

public class HongKongChocolateIceCream extends IceCream {
    public HongKongChocolateIceCream() {
        this.setName("Hong Kong styled Ice Cream");
        this.setFlavor("Chocolate");
        this.setType("Egg Waffle");
    }
}
