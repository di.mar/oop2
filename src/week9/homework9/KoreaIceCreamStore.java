package week9.homework9;

public class KoreaIceCreamStore extends IceCreamStore {

    @Override
    protected IceCream makeIceCream(String type) {
    if (type.equals("strawberry")){
        return new KoreaStrawberryIceCream();
    }
        if (type.equals("caramel")){
            return new KoreaCaramelIceCream();
        }
        if (type.equals("chocolate")){
            return new KoreaChocolateIceCream();
        }
    else{
        return null;
    }
    }
}
