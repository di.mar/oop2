package week9.homework9;



public class HongKongIceCreamStore extends IceCreamStore {
    @Override
    protected IceCream makeIceCream( String type) {
        if (type.equals("strawberry")){
         return new HongKongStrawberryIceCream();
    }
        else if(type.equals("Caramel")){
            return new HongKongCaramelIceCream();
        }
        else if(type.equals("Chocolate")){
            return new HongKongChocolateIceCream();
        }
        else {
        return null;
        }
    }
}
