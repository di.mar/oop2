package week9.homework9;

public abstract class IceCream {
    private String name;
    private String flavor;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlavor() {
        return flavor;
    }

    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void preparing(){
        System.out.println("Ice Cream name is: "+this.getName());
        System.out.println("Its flavor is: " + this.getFlavor());
        System.out.println("Its type is: "+this.getType());

    }
    public void freezing(){
        System.out.println("It is freezed with -35*C degree.");
    }

    public void packaging(){
        System.out.println("It packaged into Ice Cream Store package.");
    }
}
