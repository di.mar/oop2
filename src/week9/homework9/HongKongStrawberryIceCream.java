package week9.homework9;

public class HongKongStrawberryIceCream extends IceCream {
    public HongKongStrawberryIceCream() {
        this.setName("Hong Kong styled Ice Cream");
        this.setFlavor("Strawberry");
        this.setType("Egg Waffle");
    }
}
