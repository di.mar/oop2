package week9.homework9;

public class KoreaStrawberryIceCream extends IceCream {
    public KoreaStrawberryIceCream() {
        this.setName("Korea styled Ice Cream");
        this.setFlavor("Strawberry");
        this.setType("Pancake");
    }
}
