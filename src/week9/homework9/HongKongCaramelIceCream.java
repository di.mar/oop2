package week9.homework9;

public class HongKongCaramelIceCream extends IceCream {
    public HongKongCaramelIceCream() {
        this.setName("Hong Kong styled Ice Cream");
        this.setFlavor("Caramel");
        this.setType("Egg Waffle");
    }
}
