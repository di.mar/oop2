package week9.homework9;

public class KoreaCaramelIceCream extends IceCream {
    public KoreaCaramelIceCream() {
        this.setName("Korea styled Ice Cream");
        this.setFlavor("Caramel");
        this.setType("Pancake");
    }
}
