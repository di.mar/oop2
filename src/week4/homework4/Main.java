package week4.homework4;

public class Main {
    public static void main(String[] args) {
        Message mes = new Message("Aidyn");
        mes.hasPhoneNum();
        mes.hasInternet();
        try {
            mes.send();
        } catch (MessageIsNotSent messageIsNotSent) {
            messageIsNotSent.printStackTrace();
        }
    }
}
