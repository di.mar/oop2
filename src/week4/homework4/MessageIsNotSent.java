package week4.homework4;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class MessageIsNotSent extends Exception{
    public MessageIsNotSent(String message) {
        super(message);

        PrintWriter out = null;
        try {
            FileOutputStream myFile = new FileOutputStream("message.txt");
            out = new PrintWriter(myFile);
            out.print(message);
            out.close();
        } catch (IOException e) {
            System.out.println("Don't write date to File");
        } finally {
            out.close();
        }
}}
