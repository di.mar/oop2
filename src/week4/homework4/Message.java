package week4.homework4;

public class Message {
    private String receiver;
    private boolean havePhoneNum;
    private boolean haveInternet;

    public Message(String receiver) {
        this.receiver = receiver;
    }

    public String getReceiver() {
        return receiver;
    }

    public void hasPhoneNum(){
        havePhoneNum = true;
        System.out.println("There is phone number");
    }

    public void hasInternet(){
        haveInternet = true;
        System.out.println("There is Internet");
    }

    public void send() throws MessageIsNotSent {
        if (havePhoneNum && haveInternet) {
            System.out.println("You have sent a message to " + receiver + "!");
        } else {
            throw new MessageIsNotSent (receiver + " did not get the message");
        }
    }
}
