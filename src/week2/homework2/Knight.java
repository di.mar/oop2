package week2.homework2;

public class Knight extends Character{
    public Knight() {
        w = new SwordBehavior();
    }

    @Override
    void fight() {
        System.out.println("Knight fight");
    }
}
