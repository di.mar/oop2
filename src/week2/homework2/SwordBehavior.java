package week2.homework2;

public class SwordBehavior  implements WeaponBehavior{
    @Override
    public void useWeapon() {
        System.out.println("My weapon is a sword");
    }
}
