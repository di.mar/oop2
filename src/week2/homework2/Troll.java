package week2.homework2;

public class Troll extends Character {
    public Troll() {
        w = new AxeBehavior();
    }

    @Override
    void fight() {
        System.out.println("Troll fight");
    }
}
