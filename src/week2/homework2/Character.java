package week2.homework2;

public class Character {
    WeaponBehavior w;

    void fight(){};

    public void setWeapon(WeaponBehavior weapon) {
        this.w = weapon;
    }

    public void performUseW(){
        w.useWeapon();
    }

}
