package week2.homework2;

public interface WeaponBehavior {
    void useWeapon();
}
