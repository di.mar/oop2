package week2.homework2;

public class King extends Character {
    public King() {
        w = new KnifeBehavior();
    }

    @Override
    void fight() {
        System.out.println("King fight");
    }

}
