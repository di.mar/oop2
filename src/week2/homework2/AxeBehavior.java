package week2.homework2;

public class AxeBehavior implements WeaponBehavior {

    @Override
    public void useWeapon() {
        System.out.println("My weapon is an axe");
    }
}
