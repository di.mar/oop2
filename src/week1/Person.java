package week1;

public class Person {
    private String name;
    private int birthYear;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public Person(String name, int birthYear) {
        this.name = name;
        this.birthYear = birthYear;
    }

    public Person() {
    }

    public int age(){
        int age = 2020-this.birthYear;
        return age;
    }
    public void input(String name, int BirthYear){
        this.name = name;
        this.birthYear = BirthYear;
    }
    public void output(){
        System.out.println("Name: "+this.name);
        System.out.println("Birth Year: "+this.birthYear);
    }

    public void changeName( String name){
        this.name =name;
    }
}
