package week1;

public class Main {
    public static void main(String[] args) {
          //task1
        Employee first = new Employee("Diana", 10, 5);
        Employee second = new Employee("Alisher", 20, 6);
        Employee third = new Employee("Aibek",30,7);

        System.out.println(first.getHours());
        System.out.println(second.getHours());
        System.out.println(third.getHours());

        //task2

        Person person1 = new Person();
        Person person2 = new Person();
        Person person3 = new Person();
        Person person4 = new Person();
        Person person5 = new Person();

        person1.input("Aidos", 1997);
        person2.input("Eldos", 1998);
        person3.input("Erdos", 1999);
        person4.input("Arman", 2000);
        person5.input("Arlan", 2001);
    }
}
