package week1;

public class Employee {
    private String name;
    private int rate;
    private int hours;

    static int TotalSum;

    public static int getTotalSum() {
        return TotalSum;
    }

    public static void setTotalSum(int totalSum) {
        TotalSum = totalSum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public Employee() {
    }

    public Employee(String name, int rate) {
        this.name = name;
        this.rate = rate;
    }

    public Employee(String name, int rate, int hours) {
        this.name = name;
        this.rate = rate;
        this.hours = hours;
    }

    public int salary(){
        int salary = this.rate * this.hours;
        return salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", rate=" + rate +
                ", hours=" + hours +
                '}';
    }

    public void changeRate(int rate){
        this.rate = rate;
    }

    public int bonuses(){
        int salary = this.salary();
        int bonuses = salary /10;
        return bonuses;
    }
}
